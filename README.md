# Utilisation de scripts Python en lignes de commande

![image tuyau](https://cdn.pixabay.com/photo/2017/03/27/13/55/lost-places-2178884_1280.jpg)

## Recherche d'informations

- Comment peut-on passer des arguments à un script Python ?

- Qu'existe-t-il dans la librairie standard de Python pour effectuer ceci ? Dans des librairies externes ?

## Activité

- Importer le module `argparse` de la biliothèque standard de Python dans le fichier `script_python.py`

- Créer un parseur à l'aide de la classe `ArgumentParser`du module `argparse`

- Ajouter un argument de type chaîne de caractères au parseur à l'aide de la méthode `add_argument()` du parseur. Ajouter un message d'aide

- Créer une variable pour stocker la donnée transmise en argument du script à l'aide de la méthode `parse_args()`

- Afficher la valeur de l'argument à l'aide de la fonction `print()`

- Vérifier le fonctionnement du script avec un argument à l'aide de la commande : `python3 script_python.py "essai script python"`

- Vérifier le bon fonctionnement des messages d'aide (description du parseur et messages d'aide sur les arguments)

- Ajouter un argument optionnel de type integer au parseur à l'aide de la méthode `add_argument()` du parseur. Ajouter un message d'aide

- Vérifier son bon fonctionnement. Essayer de passer une chaîne de caractères à cet argument optionnel

- Ajouter un argument optionnel de type booléen au parseur à l'aide de la méthode `add_argument()` du parseur. Ajouter un message d'aide. Ajouter une condition dans le code pour afficher un message différent suivant que l'option est présente ou non.

- Vérifier son bon fonctionnement

- Ajouter un argument optionnel de type integer en n'autorisant que 3 valeur "1", "5" et "7" au parseur à l'aide de la méthode `add_argument()` du parseur. Ajouter un message d'aide.

- Vérifier son bon fonctionnement

## Références :

- https://docs.python.org/3/library/argparse.html

- https://docs.python.org/fr/3/howto/argparse.html

- https://click.palletsprojects.com/en/7.x/